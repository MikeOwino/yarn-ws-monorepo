const PROJECT = {
  title: "Site Title",
  subtitle: "Wow this is Nice",
};

export function getSiteInfo() {
  return { title: PROJECT.title, subtitle: PROJECT.subtitle };
}
